#include "net.h"
#include "ip.h"
#include "ethernet.h"
#include "arp.h"
#include "icmp.h"

/**
 * @brief 处理一个收到的数据包
 * 
 * @param buf 要处理的数据包
 * @param src_mac 源mac地址
 */


void ip_in(buf_t *buf, uint8_t *src_mac)
{
    // TO-DO

    if ( buf->len < sizeof(ip_hdr_t)) return; // check head len 

    ip_hdr_t * ip_hdr = (ip_hdr_t *) buf->data;

    if ( ip_hdr->version != IP_VERSION_4 || swap16(ip_hdr->total_len16) > buf->len) return; // check head 

    uint16_t checksum = ip_hdr->hdr_checksum16;
    ip_hdr->hdr_checksum16 = 0 ;
    if (checksum != checksum16(( uint16_t * )buf->data,sizeof(ip_hdr_t)))
        return; // check sum 

    ip_hdr->hdr_checksum16 = checksum;
    if (memcmp(ip_hdr->dst_ip, net_if_ip, 4) != 0) return;

    if (buf->len >swap16(ip_hdr->total_len16)){
        buf_remove_padding(buf,buf->len - swap16(ip_hdr->total_len16 ));
    }

    if (ip_hdr->protocol == NET_PROTOCOL_ARP || ip_hdr->protocol == NET_PROTOCOL_IP
        || ip_hdr->protocol == NET_PROTOCOL_ICMP || ip_hdr->protocol == NET_PROTOCOL_UDP
        ){
        buf_remove_header(buf, sizeof(ip_hdr_t));
        net_in(buf,ip_hdr->protocol,ip_hdr->src_ip);
    }
    else{
        icmp_unreachable(buf,ip_hdr->src_ip,ICMP_CODE_PROTOCOL_UNREACH);
    }
}

/**
 * @brief 处理一个要发送的ip分片
 * 
 * @param buf 要发送的分片
 * @param ip 目标ip地址
 * @param protocol 上层协议
 * @param id 数据包id
 * @param offset 分片offset，必须被8整除
 * @param mf 分片mf标志，是否有下一个分片
 */
void ip_fragment_out(buf_t *buf, uint8_t *ip, net_protocol_t protocol, int id, uint16_t offset, int mf)
{
    // TO-DO

    buf_add_header(buf, sizeof(ip_hdr_t));

    ip_hdr_t *ip_hdr = (ip_hdr_t * ) buf->data;

    ip_hdr->hdr_len = sizeof(ip_hdr_t ) / IP_HDR_LEN_PER_BYTE ;      
    ip_hdr->version = IP_VERSION_4;      
    ip_hdr->tos     = 0;              
    ip_hdr->total_len16 = swap16(buf->len);    
    ip_hdr->id16     =  swap16(id);           
    ip_hdr->flags_fragment16 = mf ? swap16(IP_MORE_FRAGMENT | (offset >> 3)) : swap16(offset >> 3);
    ip_hdr->ttl      = IP_DEFALUT_TTL;              
    ip_hdr->protocol = protocol;
    memcpy(ip_hdr->dst_ip,ip,NET_IP_LEN);    
    memcpy(ip_hdr->src_ip,net_if_ip,NET_IP_LEN); 

    ip_hdr->hdr_checksum16 = 0 ;
    ip_hdr->hdr_checksum16 = checksum16(( uint16_t * )buf->data,sizeof(ip_hdr_t));

    arp_out(buf,ip);

}

/**
 * @brief 处理一个要发送的ip数据包
 * 
 * @param buf 要处理的包
 * @param ip 目标ip地址
 * @param protocol 上层协议
 */
void ip_out(buf_t *buf, uint8_t *ip, net_protocol_t protocol)
{
    // TO-DO
    static int id = 0;
    if ( buf->len >  1500 - sizeof(ip_hdr_t) ){
        int ip_head_offset = sizeof(ip_hdr_t);
        int frag_len = (ETHERNET_MAX_TRANSPORT_UNIT  - ip_head_offset) / 8 * 8;

        int shard_num = (buf->len + frag_len -1 )/ frag_len;


        for ( int i =0 ; i< shard_num - 1 ;i++){
            buf_t  frag_buf;
            buf_init(&frag_buf,frag_len);
            memcpy(frag_buf.data , buf->data + i*frag_len  , frag_len);
           
            ip_fragment_out(&frag_buf,ip,protocol,id,i*frag_len  ,1);
        }
        buf_t  frag_buf ;
        buf_init(&frag_buf,buf->len% frag_len);
        memcpy(frag_buf.data , buf->data + (shard_num -1 )*frag_len  ,buf->len % frag_len);
        
        ip_fragment_out(&frag_buf,ip,protocol,id,(shard_num -1 )*frag_len  ,0);

    }else{
        ip_fragment_out(buf,ip,protocol,id,0,0);
    }
    id++ ;
}

/**
 * @brief 初始化ip协议
 * 
 */
void ip_init()
{
    net_add_protocol(NET_PROTOCOL_IP, ip_in);
}